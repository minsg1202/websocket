package com.websocket.chatting.contoller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 소켓 메시지 컨트롤러
 * @since   2019. 11. 28
 * @author  민승기
 * @history 민승기 [2019. 11. 28] [최초 작성]
 */
@Controller
public class ChattingController {

  /**
   * 채팅 페이지 메인
   * @param request - HttpServletRequest
   * @param response - HttpServletResponse
   * @return ModelAndView
   */
  @GetMapping(value = "/main")
  public String chat (HttpServletRequest request, HttpServletResponse response) {
    return "chat";
  }
}
